import supabase from '../supabase';

function Todos() {
  async function handleSubmit(event) {
    event.preventDefault();
    const { title, completed } = event.target.elements;

    const { data, error } = await supabase.from('todos').insert({
      title: title.value,
      completed: completed.checked,
    });
  }

  return (
    <div className="page-container">
      <h1>Todo list</h1>
      <form onSubmit={handleSubmit}>
        <input
          id="title"
          placeholder="What do you want to do?"
          aria-label="Todo name"
        />
        <input id="completed" type="checkbox" aria-label="Completed" />
        <button>Add</button>
      </form>
    </div>
  );
}

export default Todos;
