import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import supabase from '../supabase';

function Login() {
  const [submitting, setSubmitting] = useState(false);
  const navigate = useNavigate();

  async function handleSubmit(event) {
    event.preventDefault();
    const { email, password } = event.target.elements;

    setSubmitting(true);
    const { error } = await supabase.auth.signInWithPassword({
      email: email.value,
      password: password.value,
    });
    setSubmitting(false);

    if (!error) {
      navigate('/');
    }
  }

  return (
    <div className="page-container">
      <h1>Login</h1>
      <form autoComplete="off" onSubmit={handleSubmit} className="form">
        <input
          id="email"
          type="email"
          placeholder="Email address"
          aria-label="Email address"
        />
        <input
          id="password"
          type="password"
          placeholder="Password"
          aria-label="Your password"
        />
        <button disabled={submitting}>
          {submitting ? 'Signing you up...' : 'Sign Up'}
        </button>
      </form>
    </div>
  );
}

export default Login;
