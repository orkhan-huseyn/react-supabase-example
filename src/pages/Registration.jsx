import { useState } from 'react';
import supabase from '../supabase';

function Registration() {
  const [submitting, setSubmitting] = useState(false);

  async function handleSubmit(event) {
    event.preventDefault();
    const { email, password } = event.target.elements;

    setSubmitting(true);
    const { data, error } = await supabase.auth.signUp({
      email: email.value,
      password: password.value,
    });

    setSubmitting(false);
  }

  return (
    <div className="page-container">
      <h1>Registration</h1>
      <form autoComplete="off" onSubmit={handleSubmit} className="form">
        <input
          id="email"
          type="email"
          placeholder="Email address"
          aria-label="Email address"
        />
        <input
          id="password"
          type="password"
          placeholder="Password"
          aria-label="Your password"
        />
        <button disabled={submitting}>
          {submitting ? 'Signing you up...' : 'Sign Up'}
        </button>
      </form>
    </div>
  );
}

export default Registration;
