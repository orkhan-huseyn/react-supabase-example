import { useCountries } from '../contexts/CountriesContext';

function Dashboard() {
  const countries = useCountries();

  return (
    <div className="page-container">
      <h1>Total number of countries: {countries.length}</h1>
    </div>
  );
}

export default Dashboard;
