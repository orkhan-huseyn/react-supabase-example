import { useCountries } from '../contexts/CountriesContext';

function Countries() {
  const countries = useCountries();

  return (
    <div className="page-container">
      <h1>Countries</h1>
      <ul>
        {countries.map((country) => (
          <li key={country.id}>{country.name}</li>
        ))}
      </ul>
    </div>
  );
}

export default Countries;
