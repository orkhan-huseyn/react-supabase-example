import { NavLink, Route, Routes } from 'react-router-dom';
import Countries from './pages/Countries';
import Login from './pages/Login';
import Registration from './pages/Registration';
import Dashboard from './pages/Dashboard';
import CountriesProvider from './contexts/CountriesContext';
import DefaultLayout from './layouts/DefaultLayout';
import Todos from './pages/Todos';

function App() {
  return (
    <CountriesProvider>
      <Routes>
        <Route path="/" element={<DefaultLayout />}>
          <Route index element={<Dashboard />} />
          <Route path="/countries" element={<Countries />} />
          <Route path="/todos" element={<Todos />} />
        </Route>
        <Route path="/login" element={<Login />} />
        <Route path="/registration" element={<Registration />} />
      </Routes>
    </CountriesProvider>
  );
}

export default App;
