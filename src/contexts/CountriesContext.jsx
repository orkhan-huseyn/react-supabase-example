import { createContext, useContext, useEffect, useReducer } from 'react';
import supabase from '../supabase';
import { reducer } from './reducer';

const CountriesContext = createContext();

export function useCountries() {
  return useContext(CountriesContext);
}

function CountriesProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, {
    countries: [],
    loading: true,
    error: '',
  });

  useEffect(() => {
    dispatch({ type: 'loading_start' });
    supabase
      .from('countries')
      .select()
      .then((response) => {
        dispatch({ type: 'response_success', data: response.data });
      })
      .catch((error) => {
        dispatch({ type: 'response_error', error: error });
      })
      .finally(() => {
        dispatch({ type: 'loading_stop' });
      });
  }, []);

  return (
    <CountriesContext.Provider value={state.countries}>
      {children}
    </CountriesContext.Provider>
  );
}

export default CountriesProvider;
