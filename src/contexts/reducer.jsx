export function reducer(state, action) {
  switch (action.type) {
    case 'loading_start':
      return {
        loading: true,
        countries: [],
        error: '',
      };
    case 'loading_stop':
      return {
        loading: false,
        countries: state.countries,
        error: state.error,
      };
    case 'response_success':
      return {
        loading: state.loading,
        countries: action.data,
        error: state.error,
      };
    case 'response_error':
      return {
        loading: state.loading,
        countries: [],
        error: action.error,
      };
    default:
      return state;
  }
}
