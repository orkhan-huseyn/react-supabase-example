import { useEffect } from 'react';
import { NavLink, Outlet, useNavigate } from 'react-router-dom';
import supabase from '../supabase';

function DefaultLayout() {
  const navigate = useNavigate();

  useEffect(() => {
    supabase.auth.getSession().then((response) => {
      const { data, error } = response;
      if (!data.session || error) {
        navigate('/login');
      }
    });
  }, []);

  return (
    <>
      <header>
        <nav>
          <NavLink to="/">Dashboard</NavLink>
          <NavLink to="/countries">Countries</NavLink>
          <NavLink to="/todos">Todos</NavLink>
        </nav>
      </header>
      <main>
        <Outlet />
      </main>
    </>
  );
}

export default DefaultLayout;
